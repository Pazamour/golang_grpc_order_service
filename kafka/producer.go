package kafka

import (
	"encoding/json"
	"fmt"
	proto "gRPC-Order-Service/proto/generated"
	"github.com/confluentinc/confluent-kafka-go/kafka"
)

func SaveOrderToKafka(order proto.Order) {

	fmt.Println("Save Order To Kafka")

	jsonString, err := json.Marshal(order)
	if err != nil {
		panic(err)
	}
	orderString := string(jsonString)

	topic := "orders-topic"

	p,err := kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": "localhost:9092"})
	if err != nil {
		panic(err)
	}

	p.Produce(&kafka.Message{
		TopicPartition: kafka.TopicPartition{Topic: &topic, Partition:kafka.PartitionAny},
		Value: []byte(orderString),
	}, nil)
	// Produce messages to topic
	/*for _, word := range []string{orderString} {
		p.Produce(&kafka.Message{
			TopicPartition: kafka.TopicPartition{Topic: &topic, Partition:kafka.PartitionAny},
			Value: []byte(word),
		}, nil)
	}*/
}
