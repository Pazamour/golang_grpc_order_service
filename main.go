package main

import (
	"gRPC-Order-Service/server"
)

func main() {
	server.StartServer(&server.BasicServer{Name: "main"})
}
