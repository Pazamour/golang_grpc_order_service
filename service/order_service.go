package service

import (
	"context"
	"fmt"
	"gRPC-Order-Service/cache"
	"gRPC-Order-Service/kafka"
	pb "gRPC-Order-Service/proto/generated"
	"gRPC-Order-Service/repository"
	"math/rand"
	"reflect"
	"strconv"

	//"google.golang.org/protobuf/runtime/protoimpl"
	"log"
)

type IOrderService interface {
	// Create new order
	Create(context.Context, *pb.CreateRequest) (*pb.CreateResponse, error)
	// Find order
	FindById(context.Context, *pb.FindRequest) (*pb.FindResponse, error)
	// Update order
	Update(context.Context, *pb.UpdateRequest) (*pb.UpdateResponse, error)
	// Delete order
	Delete(context.Context, *pb.DeleteRequest) (*pb.DeleteResponse, error)
	// Find all order
	FindAll(context.Context, *pb.FindAllRequest) (*pb.FindAllResponse, error)
}

type OrderService struct {
}

func NewOrderService(repository repository.OrdersRepository, cache cache.OrdersCache) *OrderService {
	repo = repository
	orders_cache = cache
	o := &OrderService{}
	return o
}

var (
	repo repository.OrdersRepository
	orders_cache cache.OrdersCache
)

// Create new order
func (s *OrderService) Create(ctx context.Context, req *pb.CreateRequest) (*pb.CreateResponse, error) {
	log.Println("Received Create Request. Req: ", req)


	order := req.GetOrder()
	id := rand.Intn(200)
	order.Id = strconv.Itoa(id)
	kafka.SaveOrderToKafka(*order)
	_, err := repo.Save(order)
	if err != nil {
		return nil, err
	}
	cr := pb.CreateResponse{
		Id: (int64(id)),
	}
	return &cr, nil
}

// Find order
func (s *OrderService) FindById(ctx context.Context, req *pb.FindRequest) (*pb.FindResponse, error) {
	log.Println("Received FindById Request. Req: ", req)

	id := strconv.Itoa(int(req.GetId()))
	_, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		return nil, err
	}

	order1 := orders_cache.Get(id)

	if order1 == nil {
		order, err := repo.FindById(id)
		if err != nil {
			return nil, err
		}

		orders_cache.Set(id, order)
		fr := pb.FindResponse{Order:order}
		return &fr, nil
	} else {
		fr := pb.FindResponse{Order:order1}
		return &fr, nil
	}
}

// Update order
func (s *OrderService) Update(ctx context.Context, req *pb.UpdateRequest) (*pb.UpdateResponse, error) {
	log.Println("Received Update Request. Req: ", req)

	oldOrderId := req.GetOrder().GetId()
	oldOrder, err := repo.FindById(oldOrderId)
	if err != nil {
		return nil, err
	}

	err1 := repo.Delete(oldOrder)
	if err1 != nil {
		return &pb.UpdateResponse{Order:oldOrder}, err1
	}

	newOrder := req.GetOrder()
	order, err := repo.Save(newOrder)
	kafka.SaveOrderToKafka(*newOrder)
	if err != nil {
		return nil, err
	}

	ur := pb.UpdateResponse{Order:order}
	return &ur, nil
}

// Delete order
func (s *OrderService) Delete(ctx context.Context, req *pb.DeleteRequest) (*pb.DeleteResponse, error) {
	log.Println("Received Delete Request. Req: ", req)

	orderId := strconv.Itoa(int(req.GetId()))
	order, err := repo.FindById(orderId)
	if err != nil {
		return nil, err
	}
	fmt.Println(" ", orderId, reflect.TypeOf(orderId))
	err = repo.Delete(order)
	if err != nil {
		return nil, err
	}

	return &pb.DeleteResponse{}, nil
}

// Find all order
func (s *OrderService) FindAll(ctx context.Context, req *pb.FindAllRequest) (*pb.FindAllResponse, error) {
	log.Println("Received FindAll Request. Req: ", req)

	orders,err := repo.FindAll()
	if err != nil {
		return nil, err
	}

	far := pb.FindAllResponse{Orders : orders}
	return &far, nil
	/*
	far := pb.FindAllResponse{Orders: []*pb.Order{{Id : "100", ItemName: "Chocolate", Price:"200"},{Id : "101", ItemName: "Pizza", Price:"300"}}}
	return &far, nil
	 */
}

