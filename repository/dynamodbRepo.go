package repository

import (
	"fmt"
	proto "gRPC-Order-Service/proto/generated"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

type dynamoDBRepository struct {
	tableName string
}

func NewDynamoDBRepository() OrdersRepository {
	return &dynamoDBRepository{
		tableName: "Orders1234",
	}
}

func createDynamoDBClient() *dynamodb.DynamoDB {
	// Create an AWS session
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))

	return dynamodb.New(sess)
}

func (repo *dynamoDBRepository) Save(order *proto.Order) (*proto.Order, error) {
	dynamoDBClient := createDynamoDBClient()

	// Transforms the order to map[string]*dynamodb.AttributeValue
	attributeValue, err := dynamodbattribute.MarshalMap(order)

	if err != nil {
		return nil, err
	}

	fmt.Println("marshalled struct: %+v", attributeValue)
	//fmt.Println("Order:%v", order)
	//fmt.Println("In DynamoDB Repo: order id:%v, ItemName:%v, pricd: %v, tableName:%v", order.Id, order.ItemName, order.Price, repo.tableName)

	// Create the Item Input
	item := &dynamodb.PutItemInput{
		Item:      attributeValue,
		TableName: aws.String(repo.tableName),
	}

	// Save the Item into DynamoDB
	_, err = dynamoDBClient.PutItem(item)
	if err != nil {
		fmt.Println("Got error calling PutItem:")
		fmt.Println(err.Error())
		return nil, err
	}

	return order, err

}

func (repo *dynamoDBRepository) FindAll() ([]*proto.Order, error) {
	dynamoDBClient := createDynamoDBClient()

	// Build the query input parameters
	params := &dynamodb.ScanInput{
		TableName: aws.String(repo.tableName),
	}

	// Make the DynamoDB Query API call
	result, err := dynamoDBClient.Scan(params)
	if err != nil {
		return nil, err
	}

	var orders []*proto.Order = []*proto.Order{}

	for _, i := range result.Items {
		order := proto.Order{}

		err = dynamodbattribute.UnmarshalMap(i, &order)

		if err != nil {
			panic(err)
		}
		orders = append(orders, &order)
	}
	return orders, nil

}

func (repo *dynamoDBRepository) FindById(id string) (*proto.Order, error) {
	dynamoDBClient := createDynamoDBClient()

	result, err := dynamoDBClient.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(repo.tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"Id": {
				S: aws.String(id),
			},
		},
	})

	//fmt.Println("In repo after find:", result)
	if err != nil {
		return nil, err
	}
	order := proto.Order{}
	err = dynamodbattribute.UnmarshalMap(result.Item, &order)
	if err != nil {
		panic(err)
	}
	return &order, nil

}

// TODO : Delete
func (repo *dynamoDBRepository) Delete(order *proto.Order) error {
	dynamoDBClient := createDynamoDBClient()

	input := &dynamodb.DeleteItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"Id": {
				S: aws.String(order.Id),
			},
		},
		TableName: aws.String(repo.tableName),
	}

	_, err := dynamoDBClient.DeleteItem(input)
	if err != nil {
		fmt.Println("Got error calling DeleteItem")
		fmt.Println(err.Error())
		return err
	}

	return nil
}

/*
// TODO : Update
func (repo *dynamoDBRepository) Update(order *proto.Order) (*proto.Order, error) {
	dynamoDBClient := createDynamoDBClient()

	input := &dynamodb.UpdateItemInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":pr": {
				N: aws.String(order.Price),
			},
		},
		TableName: aws.String(repo.tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"Id": {
				N: aws.String(string(order.Id)),
			},
			"ItemName": {
				S: aws.String(order.ItemName),
			},
		},
		ReturnValues:     aws.String("UPDATED_NEW"),
		UpdateExpression: aws.String("set Price = :pr"),
	}

	_, err := dynamoDBClient.UpdateItem(input)
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	return order,nil
}
*/
