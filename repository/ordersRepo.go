package repository

import proto "gRPC-Order-Service/proto/generated"

type OrdersRepository interface {
	Save(order *proto.Order) (*proto.Order, error)
	FindAll() ([]*proto.Order, error)
	FindById(id string) (*proto.Order, error)
	Delete(order *proto.Order) error
	//Update(order *proto.Order) (*proto.Order, error)
}
