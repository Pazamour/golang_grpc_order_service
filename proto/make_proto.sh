#!/usr/bin/env bash

#generate-grpc:
protoc -I proto -I proto/third_party/googleapis --go_out=plugins=grpc,paths=source_relative:./proto/generated --grpc-gateway_out=./proto/generated proto/order.proto
