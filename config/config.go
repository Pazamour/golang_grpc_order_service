package config

import (
	"log"
	"os"
	"strconv"
)

const (
	GRPC_ENDPOINT                      = "GRPC_ENDPOINT"
	HTTP_ENDPOINT                      = "HTTP_ENDPOINT"
	REDIS_ENDPOINT					   = "REDIS_ENDPOINT"
)

var DEFAULT_CONFIG = map[string]string{
	GRPC_ENDPOINT:                         "localhost:9090",
	HTTP_ENDPOINT:                         "localhost:8080",
	REDIS_ENDPOINT:						   "localhost:6379",
}

func GetEnv(key string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	} else if fallback, exists := DEFAULT_CONFIG[key]; exists {
		return fallback
	}
	log.Println("Could not find config: ", key)
	return ""
}

func GetEnvBool(key string) bool {
	if value, ok := os.LookupEnv(key); ok {
		boolValue, err := strconv.ParseBool(value)
		if err != nil {
			log.Println(err)
		} else {
			return boolValue
		}
	} else if fallback, exists := DEFAULT_CONFIG[key]; exists {
		boolValue, err := strconv.ParseBool(fallback)
		if err != nil {
			log.Println(err)
		} else {
			return boolValue
		}
	}
	log.Println("Could not find config: ", key)
	return false
}

func GetEnvInt(key string) int {
	if value, ok := os.LookupEnv(key); ok {
		intValue, err := strconv.Atoi(value)
		if err != nil {
			log.Println(err)
		} else {
			return intValue
		}
	} else if fallback, exists := DEFAULT_CONFIG[key]; exists {
		intValue, err := strconv.Atoi(fallback)
		if err != nil {
			log.Println(err)
		} else {
			return intValue
		}
	}
	log.Println("Could not find config: ", key)
	return 0
}