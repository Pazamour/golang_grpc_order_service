package server

import (
	"context"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"time"
)

type Server struct {
}

type ServerInterface interface {
	startGRPCServer(grpcServer grpcServerInterface, lis net.Listener)
	startHttpProxyServer(srv httpServerInterface)
	prepareGracefulShutdown(grpcServer grpcServerInterface, srv httpServerInterface)
}

type grpcServerInterface interface {
	Serve(lis net.Listener) error
	GracefulStop()
}

type httpServerInterface interface {
	ListenAndServe() error
	Shutdown(ctx context.Context) error
}

func (s *Server) prepareGracefulShutdown(grpcServer grpcServerInterface, srv httpServerInterface) {
	// Wait for interrupt signal to gracefully shutdown the server with
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Starting Shutdown..")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	grpcServer.GracefulStop()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Error Stopping Server:", err)
	}
	time.Sleep(2 * time.Second) // giving time for consumers and other async jobs to shut down
	log.Println("Ending Shutdown.")
}

func (s *Server) startGRPCServer(grpcServer grpcServerInterface, lis net.Listener) {
	go func() {
		log.Println("Starting gRPC Server...")
		if err := grpcServer.Serve(lis); err != nil {
			log.Printf("failed to serve: %v", err)
		}
	}()
}

func (s *Server) startHttpProxyServer(srv httpServerInterface) {
	go func() {
		log.Println("Starting HTTP Proxy Server...")
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Printf("listen: %s\n", err)
		}
	}()
}

