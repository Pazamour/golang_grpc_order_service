package server

import (
	"context"
	"flag"
	"gRPC-Order-Service/config"
	"gRPC-Order-Service/handlers"
	pb "gRPC-Order-Service/proto/generated"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"google.golang.org/grpc"
	"log"
	"net"
	"net/http"
)

type BasicServer struct {
	Name    string
	IsUp    bool
	Handler handlers.HandlerInterface
}

type BasicServerInterface interface {
	Start(opts []grpc.ServerOption, handler handlers.HandlerInterface, server ServerInterface)
}

var (
	grpcServerEndpoint = flag.String("grpc-server-endpoint", config.GetEnv(config.GRPC_ENDPOINT), "gRPC server endpoint")
	httpServerEndpoint = flag.String("http-server-endpoint", config.GetEnv(config.HTTP_ENDPOINT), "http Server endpoint")
)

func StartServer(server BasicServerInterface) {
	var opts []grpc.ServerOption
	handler := handlers.NewHandler()
	server.Start(opts, handler, &Server{})
}

func (s *BasicServer) Start(opts []grpc.ServerOption, serverHandler handlers.HandlerInterface, server ServerInterface) {
	// Starting gRPC server
	lis, err := net.Listen("tcp", *grpcServerEndpoint)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	grpcServer := grpc.NewServer(opts...)
	pb.RegisterOrderServiceServer(grpcServer, serverHandler.GetOrderServiceServer())
	server.startGRPCServer(grpcServer, lis)

	// Starting HTTP Server
	log.Println("Starting http server...")
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	// Register gRPC server endpoints
	mux := runtime.NewServeMux(runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.JSONPb{}))

	//runtime.SetHTTPBodyMarshaler(mux)
	dialOpts := []grpc.DialOption{grpc.WithInsecure()}
	err = pb.RegisterOrderServiceHandlerFromEndpoint(ctx, mux, *grpcServerEndpoint, dialOpts)
	if err != nil {
		log.Fatal(err)
	}
	// Register non grpc-gateway APIs

	rmux := http.NewServeMux()
	rmux.Handle("/", mux)
	srv := &http.Server{
		Addr:    *httpServerEndpoint,
		Handler: rmux,
	}

	server.startHttpProxyServer(srv)
	s.Handler = serverHandler
	s.IsUp = true
	server.prepareGracefulShutdown(grpcServer, srv)

}