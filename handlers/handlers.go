package handlers

import (
	pb "gRPC-Order-Service/proto/generated"
)

type Handler struct {
	OrderServiceServer pb.OrderServiceServer
}

type HandlerInterface interface {
	GetOrderServiceServer() pb.OrderServiceServer
}

func NewHandler() *Handler{
	s :=  &Handler{}
	s.OrderServiceServer = NewOrderServiceHandler(s)
	return s
}

func (s *Handler) GetOrderServiceServer() pb.OrderServiceServer {
	return s.OrderServiceServer
}


