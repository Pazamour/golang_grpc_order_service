package handlers

import (
	"context"
	"flag"
	"gRPC-Order-Service/cache"
	"gRPC-Order-Service/config"
	pb "gRPC-Order-Service/proto/generated"
	"gRPC-Order-Service/repository"
	"gRPC-Order-Service/service"
)

type OrderServiceHandler struct {
	pb.UnimplementedOrderServiceServer
	server  *Handler
	service service.IOrderService
}

var (
	redisServerEndpoint = flag.String("redis-server-endpoint", config.GetEnv(config.REDIS_ENDPOINT), "Redis server endpoint")
)

func NewOrderServiceHandler(server *Handler) *OrderServiceHandler{
	s := &OrderServiceHandler{
		server:  server,
		service: service.NewOrderService(repository.NewDynamoDBRepository(), cache.NewRedisCache(*redisServerEndpoint, 10, 100)),
	}
	return s
}

func (s *OrderServiceHandler) Create(ctx context.Context, req *pb.CreateRequest) (*pb.CreateResponse, error) {
	res, err := s.service.Create(ctx, req)
	return res,err
}

func (s *OrderServiceHandler) FindById(ctx context.Context, req *pb.FindRequest) (*pb.FindResponse, error) {
	res, err := s.service.FindById(ctx, req)
	return res,err
}

func (s *OrderServiceHandler) Update(ctx context.Context, req *pb.UpdateRequest) (*pb.UpdateResponse, error) {
	res, err := s.service.Update(ctx, req)
	return res,err
}

func (s *OrderServiceHandler) Delete(ctx context.Context, req *pb.DeleteRequest) (*pb.DeleteResponse, error) {
	res, err := s.service.Delete(ctx, req)
	return res,err
}

func (s *OrderServiceHandler) FindAll(ctx context.Context, req *pb.FindAllRequest) (*pb.FindAllResponse, error) {
	res, err := s.service.FindAll(ctx, req)
	return res,err
}

