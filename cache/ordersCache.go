package cache

import (
	"gRPC-Order-Service/proto/generated"
)

type OrdersCache interface {
	Set(key string, value *proto.Order)
	Get(key string) *proto.Order
}
