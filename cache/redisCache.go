package cache

import (

	"context"
	"encoding/json"
	proto "gRPC-Order-Service/proto/generated"
	"github.com/go-redis/redis"
	"time"
)

type redisCache struct {
	host    string
	db      int
	expires time.Duration
}

func NewRedisCache(host string, db int, exp time.Duration) *redisCache {
	return &redisCache{
		host:    host,
		db:      db,
		expires: exp,
	}
}

func (cache *redisCache) getClient() *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr:     cache.host,
		Password: "",
		DB:       cache.db,
	})
}

func (cache *redisCache) Set(key string, value *proto.Order) {
	client := cache.getClient()

	// serialize order object to JSON
	json, err := json.Marshal(value)
	if err != nil {
		panic(err)
	}
	client.Set(context.TODO(), key, json, cache.expires*time.Second)
}

func (cache *redisCache) Get(key string) *proto.Order {
	client := cache.getClient()

	val, err := client.Get(context.TODO(), key).Result()

	if err != nil {
		return nil
	}

	order := proto.Order{}
	err = json.Unmarshal([]byte(val), &order)
	if err != nil {
		panic(err)
	}

	return &order

}
